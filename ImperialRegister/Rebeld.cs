﻿/*
 * Created by SharpDevelop.
 * User: joseluis.perez
 * Date: 5/9/2017
 * Time: 14:18
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;

namespace ImperialRegister
{
	/// <summary>
	/// Description of Rebeld.
	/// </summary>
	public class Rebeld {
		
		public bool Register(string rebeld, string planet) {
			bool retValue = false;
			
			if (!string.IsNullOrWhiteSpace(rebeld)) {
				if (string.IsNullOrWhiteSpace(planet)) {					
					retValue = Logger.RegisterLog(string.Format("Rebeld ({0}) on ({1}) at ({2})", rebeld, planet, System.DateTime.Now.ToString("yyyy/mm/dd hh:mm:ss") ));
				} else {
					Logger.Warning("No planet argument.");
				}
			} else {
				Logger.Warning("No Rebeld argument.");
			}
			
			return retValue;
		}
	}
}
