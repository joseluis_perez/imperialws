﻿/*
 * Created by SharpDevelop.
 * User: joseluis.perez
 * Date: 5/9/2017
 * Time: 14:16
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Data;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Collections.Generic;

namespace ImperialRegister
{
	[WebService]
	public class Soap : System.Web.Services.WebService
	{
		
		private Rebeld m_rebeld;
		
		[WebMethod]
		public bool Register(List<string> lstRegister) {
			bool retValue = false;
			
			if (lstRegister.Count != 2) {
				Logger.Warning("Invalid number of arguments.");
			} else {
				
				if (this.m_rebeld == null) 
					this.m_rebeld = new Rebeld();				
				
				retValue = this.m_rebeld.Register(lstRegister[0],lstRegister[1]);				
			}
			
			return retValue;
		}

	}
}
