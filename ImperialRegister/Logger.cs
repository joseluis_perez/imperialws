﻿/*
 * Created by SharpDevelop.
 * User: joseluis.perez
 * Date: 5/9/2017
 * Time: 14:28
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.IO;

namespace ImperialRegister
{
	/// <summary>
	/// Description of Logger.
	/// </summary>
	public class Logger
	{
		//TODO: get from config
		private static string m_WarningFile = @"WarningFile.log";
		private static string m_RegisterFile = @"RegisterFile.log";
		
		
		private static bool Write(string filename, string text) {
			bool retValue = false;
			try {
				if (!File.Exists(filename)) 
					File.Create(filename);
				
				using(StreamWriter sw = new StreamWriter(new FileStream (filename, FileMode.Append, FileAccess.Write, FileShare.ReadWrite ))) {
					sw.WriteLine(text);
				}
				retValue = true;
			} catch (IOException) {
				//TODO
			}	
			return retValue;
		}
		
		public static void Warning(string message) {						
			Write(m_WarningFile, message);
			
		}
		
		public static bool RegisterLog(string message) {
			return Write(m_RegisterFile, message);
		}
			
	}
}
